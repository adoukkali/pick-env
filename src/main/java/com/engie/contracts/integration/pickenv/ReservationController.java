package com.engie.contracts.integration.pickenv;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("contract-integration/")
public class ReservationController {

	Map<String, String> envs = new HashMap<String, String>();
	Map<String, ScheduledFuture<?>> releaseJobs = new ConcurrentHashMap<String, ScheduledFuture<?>>();
	AtomicInteger i = new AtomicInteger();
	ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	public ReservationController() {
		for (int i = 1; i <= 20; i++) {
			envs.put("int" + i, null);
		}
	}

	@GetMapping("/envs")
	public Map<String, String> getEnvs() {
		return envs;
	}

	@GetMapping("/envs/reserve")
	public synchronized String pickEnv(@RequestParam(value = "jobId", required = false) String jobId, @RequestParam(value = "bound", required = false) Boolean bound) {

		if (!bound) {
			return "int-" + UUIDGenerator.generate();
		}
		else return pickEnv(jobId);
	}

	@GetMapping("/envs/pick-one")
	public synchronized String pickEnv(@RequestParam(value = "jobId", required = false) String jobId) {
		
		for (Entry<String, String> s : envs.entrySet()) {
			if (s.getValue() == null) {
				s.setValue(jobId);
				final String envId = s.getKey();
				// schedule release after 30 min - if not released properly

				ScheduledFuture<?> releaseJob = scheduler.schedule(new Runnable() {
					@Override
					public void run() {
						System.out.println("Internal Call - Releasing env " + envId + " from jobId = " + jobId);
						release(envId, jobId);
					}
				}, 30, TimeUnit.MINUTES);

				releaseJobs.put(envId + "-" + jobId, releaseJob);
				System.out.println("API Call - Pick env " + envId + " for jobId = " + jobId);
				return envId;
			}
		}
		return "none";
	}

	@GetMapping("/envs/{envId}/release")
	public Map<String, String> release(@PathVariable("envId") String envId, @RequestParam(value = "jobId", required = false) String jobId) {
		System.out.println("API Call - Releasing env " + envId + " from jobId = " + jobId);
		if (envs.get(envId) != null && envs.get(envId).equalsIgnoreCase(jobId))
			envs.put(envId, null);

		String key = envId + "-" + jobId;
		ScheduledFuture<?> releaseJob = releaseJobs.get(key);
		if (releaseJob != null) {
			releaseJob.cancel(true);
			releaseJobs.remove(key);
		}

		return envs;
	}

}
